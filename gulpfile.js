var gulp = require('gulp');
var cache = require('gulp-cached');

var cleanCSS = require('gulp-clean-css');
var concatCss = require('gulp-concat-css');

var concatJs = require('gulp-concat');
var renameJs = require('gulp-rename');
var uglifyJs = require('gulp-uglify');
var sourcemapJs = require('gulp-sourcemaps');

var jsFiles = ['www/libs/jquery.js', 'www/js/imask.js', 'www/js/jquery-validation-1.17.0/jquery.validate.js', 'www/js/jquery-validation-1.17.0/localization/messages_uk.js',
 'www/libs/slick.js', 'www/libs/typed.js', 'www/js/main.js'];

gulp.task('css', function(){
  return gulp.src('www/css/*.css')
    .pipe(concatCss("bundle.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest('www/build'))
});

gulp.task('js', function(){
    return gulp.src(jsFiles)
        .pipe(sourcemapJs.init())
        .pipe(concatJs('concat.js'))
        .pipe(gulp.dest('www/build'))
        .pipe(renameJs('uglify.js'))
        .pipe(uglifyJs())
        .pipe(sourcemapJs.write('./map'))
        .pipe(gulp.dest('www/build'));
});


gulp.task('watch_css', function(){
  gulp.watch('www/css/*.css', ['css']);
});

gulp.task('watch_js', function(){
  gulp.watch(jsFiles, ['js']);
});

gulp.task('default', ['css', 'js', 'watch_css', 'watch_js']);