<?php
/**
 * Created by PhpStorm.
 * User: sokolov
 * Date: 17.10.2018
 * Time: 9:54
 */
?>
<!-- popup -->
<div id="popup">
    <div class="popup__body">
        <svg class="popup__body__close" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
        <p class="description hd" data-id="submit_success">
            Дякуємо що проявили інтерес до нашого продукту. Ми зв'яжемося з вами для уточнення деталей незабаром.
        </p>
        <form action="#">
            <input type="text" placeholder="Ім'я*" name="name">
            <input type="text" placeholder="Телефон*" name="phone">
            <input type="text" placeholder="E-mail*" name="email">
            <input type="text" placeholder="Назва компанії*" name="company">
            <input type="text" placeholder="Веб-сайт" name="website">
            <input type="text" placeholder="Кількість мобільних працівників*" name="employee_number">
            <textarea placeholder="Додаткові коментарі" name="comments"></textarea>
            <div class="g-recaptcha" data-sitekey="6Ld9UXUUAAAAACO2amH4bDkrYTWcHS25ULDASCCN"></div>
            <input type="hidden" name="recaptcha" />
            <button class="btn secondary-btn" data-id="submit">Відправити</button>
            <label class="error hd" data-id="submit_error">Помилка, спробуйте ще раз</label>
        </form>
    </div>
</div>
