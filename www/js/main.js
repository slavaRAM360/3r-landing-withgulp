;(function($, global) {
    var P = {
        _init: function() {
            var self = P;

            $('.slider-feedback').slick({
                dots: false,
                arrows: false,
                autoplay: true,
            });

            self.$popup = $('#popup');
            $('.popup-btn').click(self._onShowTestDriveForm);
            $('.popup__body__close').click(self._onCloseTestDriveForm);


            new IMask(
                self.$popup.find('[name="phone"]')[0], {
                    mask: '+{38}(\\000)000-00-00'
                });

            $.validator.addMethod('phone', function (value) {
                return /^\+38\(0\d{2}\)\d{3}\-\d{2}-\d{2}$/.test(value);
            }, 'Будь ласка, введіть коректний телефон.');

            $.validator.addMethod('recaptcha', function (value) {
                return !!$("#g-recaptcha-response").val();
            }, 'Підтвердіть, що Ви не робот.');

            self.$form = self.$popup.find('form');
            self.$form.validate({
                ignore: [],
                rules: {
                    name: 'required',
                    phone: {
                        'required': true,
                        'phone': true,
                    },
                    email: {
                        'required': true,
                        'email': true
                    },
                    company: 'required',
                    website: 'url',
                    employee_number: {
                        'required': true,
                        'number': true
                    },
                    recaptcha: 'recaptcha'
                },
                submitHandler: function(form, event) {
                    event.preventDefault();

                    self._onSubmit();
                }
            });
        },

        _onShowTestDriveForm: function() {
            var self = P;

            self.$popup.addClass('active');
        },

        _onCloseTestDriveForm: function() {
            var self = P;

            self.$popup.removeClass('active');
        },

        _onSubmit: function() {
            var self = P, data, $error, $success;

            data = self._getFormData();
            $error = $('[data-id="submit_error"]').hide();
            $success = $('[data-id="submit_success"]');

            $.ajax({
                url: '/test_drive.php',
                data: data,
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    if(data.success) {
                        $success.show();
                        self.$form.hide();
                    } else {
                        $error.text(data.msg).show();
                    }
                },
                error: function() {
                    $error.show();
                }
            });


            return false;
        },

        _getFormData: function() {
            var self = P;

            return {
                name: self.$form.find('[name="name"]').val(),
                phone: self.$form.find('[name="phone"]').val(),
                email: self.$form.find('[name="email"]').val(),
                company: self.$form.find('[name="company"]').val(),
                website: self.$form.find('[name="website"]').val(),
                employee_number: self.$form.find('[name="employee_number"]').val(),
                comments: self.$form.find('[name="comments"]').val(),
                recaptcha: $("#g-recaptcha-response").val()
            };
        }
    };

    $(document).ready(P._init);
})(jQuery, this);

// var typed = new Typed('#typed', {
//     stringsElement: '#typed-strings',
//     typeSpeed: 40,
//     loop: true,
// });