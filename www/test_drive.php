<?php

$recaptchaSecret = '6Ld9UXUUAAAAAONuVp66SkX0UGs2m-nB0AJEHjAE';
$recaptchaUrl = 'https://www.google.com/recaptcha/api/siteverify';

require '../backend/vendor/Request.php';
require '../backend/vendor/CHtml.php';

function isBot()
{
    global $recaptchaSecret, $recaptchaUrl;

    $isBot = true;

    $recaptcha = !empty($_POST['recaptcha']) ? $_POST['recaptcha'] : false;
    if($recaptcha) {
        $request = new Request($recaptchaUrl);
        $request->setPostFields([
            'secret' => $recaptchaSecret,
            'response' => $recaptcha,
            'remoteip' => $_SERVER['REMOTE_ADDR']
        ]);
        $request->setRequestType('POST');
        $request->execute();

        $responseBody = $request->getResponse();

        $json = json_decode($responseBody, true);
        if($json && !empty($json['success'])) {
            $isBot = false;
        }
    }

    return $isBot;
}

function sendMail()
{
    $emails = ['sokolov@ram.com.ua', 'gordiuk@ram.com.ua'];

    $name = CHtml::encode($_POST['name']);
    $phone = CHtml::encode($_POST['phone']);
    $email = CHtml::encode($_POST['email']);
    $company = CHtml::encode($_POST['company']);
    $website = CHtml::encode($_POST['website']);
    $employeeNumber = CHtml::encode($_POST['employee_number']);
    $comments = CHtml::encode($_POST['comments']);

    $subject = 'Замовити 3R';
    $message = <<<STR
    <table border="1" cellpadding="5">
    
        <tr>
            <th style="text-align:right;">Ім'я</th>
            <td>{$name}</td>
        </tr>    
        <tr>
            <th style="text-align:right;">Телефон</th>
            <td>{$phone}</td>
        </tr>    
        <tr>
            <th style="text-align:right;">E-mail</th>
            <td>{$email}</td>
        </tr>    
        <tr>
            <th style="text-align:right;">Назва компанії</th>
            <td>{$company}</td>
        </tr>    
        <tr>
            <th style="text-align:right;">Веб-сайт</th>
            <td>{$website}</td>
        </tr>    
        <tr>
            <th style="text-align:right;">Кількість мобільних працівників</th>
            <td>{$employeeNumber}</td>
        </tr>    
        <tr>
            <th style="text-align:right;">comments</th>
            <td>{$comments}</td>
        </tr>
        
    </table>

STR;

    $headers = "From: no-reply@ram.com.ua\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

    foreach($emails as $to) {
        mail($to, $subject, $message, $headers);
    }
}


if(!empty($_POST)) {
    $response = ['success' => false];

    if(!isBot()) {
        sendMail();

        $response['success'] = true;
    } else {
        $response['msg'] = 'Помилка, перевірка на СПАМ не пройдена. Спробуйте ще раз.';
    }

    print json_encode($response);
} else {
    header("HTTP/1.0 404 Not Found");
    echo "<h1>Page not found</h1>";
    exit();
}